import { Component } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
 
  public appPages = [
    { title: 'Winter', url: '/folder/Winter tracksuits', icon: 'snow' },
    { title: 'Summer', url: '/folder/Summer Tracksuits', icon: 'flame' },
    { title: 'Footwear', url: '/folder/Footwear', icon: 'paw' },
    { title: 'Kids', url: '/folder/Kids', icon: 'football' }
  ];
  public labels = ['Family', 'Friends', 'Notes', 'Work', 'Travel', 'Reminders'];
  constructor() {}
}
