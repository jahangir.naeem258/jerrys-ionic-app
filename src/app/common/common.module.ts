import { NgModule } from '@angular/core';
import { HeaderComponent } from './header/header.component';
import { SidemenuComponent } from './sidemenu/sidemenu.component';
import { BrowserModule } from '@angular/platform-browser';
// import { CommonModule } from '@angular/common';
import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';



@NgModule({
  declarations: [
    HeaderComponent, 
    SidemenuComponent
  ],
  imports: [
    BrowserModule
  ],
  exports: [
    HeaderComponent,
    SidemenuComponent
  ],
  providers: [{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy }],
})
export class CommonModule { }
